package com.cba.component.dbos.model;
/**
*
* @author sduraisamy
*/
import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class DBRequest implements Serializable{
	
	static final long serialVersionUID = 123456;
	
	private String schema;

	private String operationId;
    
    private String operationType;
    
    private String operationNameSpace;
    
    private Map<String , String> values;
    
    private List<Parameter> inputParameters;
    
    private List<Parameter> outputParameters;
    
	
    public String getSchema() {
		return schema;
	}


	public void setSchema(String schema) {
		this.schema = schema;
	}
    
	public Map<String, String> getValues() {
		return values;
	}


	public void setValues(Map<String, String> values) {
		this.values = values;
	}


	public List<Parameter> getInputParameters() {
		return inputParameters;
	}


	public void setInputParameters(List<Parameter> inputParameters) {
		this.inputParameters = inputParameters;
	}


	public List<Parameter> getOutputParameters() {
		return outputParameters;
	}


	public void setOutputParameters(List<Parameter> outputParameters) {
		this.outputParameters = outputParameters;
	}


	public String getOperationId() {
		return operationId;
	}
	
	
	public void setOperationId(String operationId) {
		this.operationId = operationId;
	}
	
	
	public String getOperationType() {
		return operationType;
	}
	
	
	public void setOperationType(String operationType) {
		this.operationType = operationType;
	}
	
	
	public String getOperationNameSpace() {
		return operationNameSpace;
	}
	
	
	public void setOperationNameSpace(String operationNameSpace) {
		this.operationNameSpace = operationNameSpace;
	}


	
}
