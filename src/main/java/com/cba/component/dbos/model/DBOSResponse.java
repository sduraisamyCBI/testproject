package com.cba.component.dbos.model;
/**
*
* @author sduraisamy
*/
import java.util.List;

public class DBOSResponse {

	private String requestId;
	
	private String originalRequestXml;
	
	private String schema;

	
	public String getOriginalRequestXml() {
		return originalRequestXml;
	}

	public void setOriginalRequestXml(String originalRequestXml) {
		this.originalRequestXml = originalRequestXml;
	}

	public String getSchema() {
		return schema;
	}

	public void setSchema(String schema) {
		this.schema = schema;
	}

	private List<DBResponse> dbResponse;

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	public List<DBResponse> getDbResponse() {
		return dbResponse;
	}

	public void setDbResponse(List<DBResponse> dbResponse) {
		this.dbResponse = dbResponse;
	}

}
