package com.cba.component.dbos.model;
/**
*
* @author sduraisamy
*/
import java.util.List;


import com.cba.component.dbos.utils.Constants;
import com.cba.trading.model.AbstractModel;


public class DBOSRequest extends AbstractModel{
	
	private String requestXml;

	private String requestId;
	
	private String replyCorrelationId;
	
	private String schema;
	
	private List<DBRequest> dbRequest;
	
    private String replyToQManager;
    
    private String replyToQueue;
    
    private String replyToChannel;
    
    private String replyToPort;
    
	public String getReplyCorrelationId() {
		return replyCorrelationId;
	}

	public void setReplyCorrelationId(String replyCorrelationId) {
		this.replyCorrelationId = replyCorrelationId;
	}

	public String getSchema() {
		return schema;
	}

	public void setSchema(String schema) {
		this.schema = schema;
	}
    
	public String getRequestXml() {
		return requestXml;
	}

	public void setRequestXml(String requestXml) {
		this.requestXml = requestXml;
	}
	
	public String getReplyToQManager() {
		return replyToQManager;
	}

	public void setReplyToQManager(String replyToQManager) {
		this.replyToQManager = replyToQManager;
	}

	public String getReplyToQueue() {
		return replyToQueue;
	}

	public void setReplyToQueue(String replyToQueue) {
		this.replyToQueue = replyToQueue;
	}

	public String getReplyToChannel() {
		return replyToChannel;
	}

	public void setReplyToChannel(String replyToChannel) {
		this.replyToChannel = replyToChannel;
	}

	public String getReplyToPort() {
		return replyToPort;
	}

	public void setReplyToPort(String replyToPort) {
		this.replyToPort = replyToPort;
	}

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	public List<DBRequest> getDbRequest() {
		return dbRequest;
	}

	public void setDbRequest(List<DBRequest> dbRequest) {
		this.dbRequest = dbRequest;
	}

	static final long serialVersionUID = 234567;
	

    public String getClassName(){
        return Constants.DBOS_REQUEST;
    }
    
    public String validateData() {
        // TODO : Validation
        String comments = "";

        if (requestId == null) comments += "Missing RequestId.\n";
        if (dbRequest == null) comments += "Missing dbRequest.\n";
        if (schema == null) comments += "Missing schema.\n";

        return comments;
    }
	@Override
	public String toString() {
		return "DBOSRequest [requestXml=" + requestXml + ", requestId="
				+ requestId + ", replyCorrelationId=" + replyCorrelationId
				+ ", schema=" + schema + ", dbRequest=" + dbRequest
				+ ", replyToQManager=" + replyToQManager + ", replyToQueue="
				+ replyToQueue + ", replyToChannel=" + replyToChannel
				+ ", replyToPort=" + replyToPort + "]";
	}
}
