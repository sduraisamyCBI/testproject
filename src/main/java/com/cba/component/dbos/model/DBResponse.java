package com.cba.component.dbos.model;
/**
*
* @author sduraisamy
*/
import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class DBResponse implements Serializable {
	static final long serialVersionUID = 4;
     private String operationId;
     
     private String operationNameSpace;
     
     private String operationType;
     
  	 private String replyCode;
     
     private String replyCodeDesc;
     
     private Integer numberOfRows;
     
     private List<String> resultSet;
     
     private Map<String,String> inParamsMap;
     
     private Map<String,String> outParamsMap;
     
     private List<Parameter> inputParameters;
     
     private List<Parameter> outputParameters;     

     private String schema;
     
     public String getSchema() {
		return schema;
	}

	public void setSchema(String schema) {
		this.schema = schema;
	}
	public Map<String, String> getInParamsMap() {
		return inParamsMap;
	}

	public void setInParamsMap(Map<String, String> inParamsMap) {
		this.inParamsMap = inParamsMap;
	}

	public Map<String, String> getOutParamsMap() {
		return outParamsMap;
	}

	public void setOutParamsMap(Map<String, String> outParamsMap) {
		this.outParamsMap = outParamsMap;
	}

	public List<Parameter> getInputParameters() {
		return inputParameters;
	}

	public void setInputParameters(List<Parameter> inputParameters) {
		this.inputParameters = inputParameters;
	}

	public List<Parameter> getOutputParameters() {
		return outputParameters;
	}

	public void setOutputParameters(List<Parameter> outputParameters) {
		this.outputParameters = outputParameters;
	}

	public String getOperationId() {
		return operationId;
	}

	public void setOperationId(String operationId) {
		this.operationId = operationId;
	}

	public String getOperationNameSpace() {
		return operationNameSpace;
	}

	public void setOperationNameSpace(String operationNameSpace) {
		this.operationNameSpace = operationNameSpace;
	}

	public String getOperationType() {
		return operationType;
	}

	public void setOperationType(String operationType) {
		this.operationType = operationType;
	}

	public String getReplyCode() {
		return replyCode;
	}

	public void setReplyCode(String replyCode) {
		this.replyCode = replyCode;
	}

	public String getReplyCodeDesc() {
		return replyCodeDesc;
	}

	public void setReplyCodeDesc(String replyCodeDesc) {
		this.replyCodeDesc = replyCodeDesc;
	}

	public Integer getNumberOfRows() {
		return numberOfRows;
	}

	public void setNumberOfRows(Integer numberOfRows) {
		this.numberOfRows = numberOfRows;
	}

	public List<String> getResultSet() {
		return resultSet;
	}

	public void setResultSet(List<String> resultSet) {
		this.resultSet = resultSet;
	}
}
