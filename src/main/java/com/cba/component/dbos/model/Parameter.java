package com.cba.component.dbos.model;
/**
*
* @author sduraisamy
*/
import java.io.Serializable;
import java.util.List;

public class Parameter implements Serializable{
	
    static final long serialVersionUID = 3;
    
	private String name;
	
	private String value;
	
	private String type;

	private List<String> values;
	
	public List<String> getValues() {
		return values;
	}

	public void setValues(List<String> values) {
		this.values = values;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
