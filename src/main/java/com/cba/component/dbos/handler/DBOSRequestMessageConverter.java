package com.cba.component.dbos.handler;

/**
 *
 * @author sduraisamy
 */
import java.io.StringReader;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;
import org.springframework.jms.support.converter.MessageConversionException;
import org.springframework.jms.support.converter.MessageConverter;

import com.cba.component.dbos.model.DBOSRequest;
import com.cba.component.dbos.model.DBRequest;
import com.cba.component.dbos.model.Parameter;
import com.cba.component.dbos.utils.Constants;
import com.cba.trading.utils.NamespaceCleaner;

public class DBOSRequestMessageConverter implements MessageConverter {

	static Logger log = Logger.getLogger(DBOSRequestMessageConverter.class.getName());
	
	private Map<String, String> xpathFieldMapping;

	public void setXpathFieldMapping(Map<String, String> xpathFieldMapping) {
		this.xpathFieldMapping = xpathFieldMapping;
	}

	public Map<String, String> getXpathFieldMapping() {
		return xpathFieldMapping;
	}

	public DBOSRequest fromMessage(Message message) throws JMSException,
			MessageConversionException {

		//
		// convert message
		//

		TextMessage msg = (TextMessage) message;
		if (message != null) {
			log.info(" Message Received --> \n " + msg.getText());
		} else {
			log.warn(" No message received");
			return null;
		}

		try {

			//
			// retrieve relevant information from request message and populate
			// model
			//
			SAXReader saxReader = new SAXReader();
			Document document = saxReader.read(new StringReader(msg.getText()));
			document.accept(new NamespaceCleaner());
			DBOSRequest requestHolder = new DBOSRequest();

			String debugString = "";
			Node node = null;
			Object value = null;
			List<DBRequest> dbRequest = null;
			
			requestHolder.setRequestXml(msg.getText());
			
			for (String key : xpathFieldMapping.keySet()) {
				value = null;
				try {
					log.info("Key -> " + key);

					if (Constants.DBREQUEST_XPATH_KEY.equals(key)) {
						dbRequest = convertRequest(document
								.selectNodes(xpathFieldMapping.get(key)));
						value = dbRequest;
					} else {
						node = document.selectSingleNode(xpathFieldMapping
								.get(key));
						if (node != null) {
							value = node.getText();
						}
					}
					log.info(key + ": " + value);
					requestHolder.set(key, value);
					debugString += key + ": " + value + "\n";
				} catch (NoSuchFieldException fe) {
					fe.printStackTrace();
					log.warn("key [" + key + "] is not a valid key");
					debugString += "key [" + key + "] is an invalid key + \n";
				} catch (IllegalAccessException iae) {
					iae.printStackTrace();
					log.warn("key [" + key + "] is not accessible");
					debugString += "key [" + key + "] is not accessible + \n";
				} catch (DocumentException de) {
					de.printStackTrace();
					log.warn("key [" + key + "] is not accessible");
					debugString += "key [" + key + "] is not accessible + \n";
				}
			}

			log.info("Printing debugString \n " + debugString);

			//
			// validate that all required info has been provided
			//
			String errorMsg = requestHolder.validateData();

			if (errorMsg.length() > 0) {
				throw new RuntimeException(errorMsg);
			}

			return requestHolder;

		} catch (DocumentException de) {
			log.error(
					"\n\n Exception caught: \n"
							+ de.getMessage()
							+ "Please verify the transaction. The request has been rejected",
					de);

			de.printStackTrace();
			throw new MessageConversionException(de.getMessage());
		}

	}

	public Message toMessage(Object obj, Session session) throws JMSException,
			MessageConversionException {

		//
		// check that object passed in is the string xml we want to put in the
		// message
		//
		String xmlMessage = "";
		if (obj instanceof String) {
			xmlMessage = (String) obj;
		} else {
			throw new MessageConversionException(
					"Object was not the xml message");
		}

		//
		// create message and return
		//
		return session.createTextMessage(xmlMessage.toString());
	}

	private List<DBRequest> convertRequest(List<Node> dbRequest)
			throws DocumentException {
		DBRequest request = null;
		log.info("Inside converRequest method");
		List<DBRequest> dbRequestList = new ArrayList<DBRequest>();
		int paramIndex = 0;
		String schema = null;
		try {
			
			for (Node node : dbRequest) {

				request = new DBRequest();

				schema =  node.valueOf(new StringBuffer(Constants.XML_ATTR_SYMBOL).append(Constants.DB_SCHEMA).toString());
				
				if (schema != null && schema.length() > 0) {
					request.setSchema(node.valueOf(new StringBuffer(Constants.XML_ATTR_SYMBOL).append(Constants.DB_SCHEMA).toString()));
				}
				
			
				request.setOperationId(node.valueOf(new StringBuffer(Constants.XML_ATTR_SYMBOL).append(Constants.OPERATION_ID).toString()));

				request.setOperationNameSpace(node
						.valueOf(new StringBuffer(Constants.XML_ATTR_SYMBOL).append(Constants.OPERATION_NAMESPACE).toString()));

				request.setOperationType(node.valueOf(new StringBuffer(Constants.XML_ATTR_SYMBOL).append(Constants.OPERATION_TYPE).toString()));

				List<Node> nodeValues = node.selectNodes(Constants.NODE_VALUE_PATH);

				Map<String, String> values = (Map<String, String>) new LinkedHashMap<String, String>();
				

				for (Node nodeValue : nodeValues) {
					values.put(paramIndex++ + "", nodeValue.getText());
				}

				paramIndex = 0;

				request.setValues(values);
				Parameter parameter = null;
				List<Parameter> inputParameterList = new ArrayList<Parameter>();
				List<Node> nodeInputParameters = node
						.selectNodes(Constants.NODE_IN_PARAMETER_PATH);
				String inputParameterType = "";
				Node inputParameterTypeNode = null;
				for (Node nodeInputParameter : nodeInputParameters) {
					// inputParameterMap.put(paramIndex++ + "",
					// nodeInputParameter.getText());
					parameter = new Parameter();
					inputParameterType = nodeInputParameter.valueOf(new StringBuffer(Constants.XML_ATTR_SYMBOL).append("type").toString());
					if (inputParameterType != null && inputParameterType.equalsIgnoreCase(Constants.NODE_MULTIPLE_PARAMETER_TYPE_VALUE))
					{
						log.info("Inside multiple params");
						log.info("Inside parameter type " + inputParameterType);
						parameter.setType(inputParameterType);
						List<Node> paramMultiple = nodeInputParameter.selectNodes(Constants.NODE_MULTIPLE_PARAMETER_VALUE_PATH);
						List<String> paramValues = new ArrayList<String>();
						log.info("param values length " + paramMultiple.size());
						for (Node nodeValue: paramMultiple)
						{
							log.info("paramValue " + nodeValue.getText());
							paramValues.add(nodeValue.getText());
						}
						parameter.setValues(paramValues);
						log.info(parameter.getValues());
					}
					else
					{	
						parameter.setName(nodeInputParameter.valueOf(new StringBuffer(Constants.XML_ATTR_SYMBOL).append(Constants.NODE_PARAMETER_ATTR_NAME).toString()));
						parameter.setValue(nodeInputParameter.getText());
					}
					inputParameterList.add(parameter);
				}
				request.setInputParameters(inputParameterList);

				paramIndex = 0;
				List<Parameter> outputParameterList = new ArrayList<Parameter>();
				List<Node> nodeOutputParameters = node
						.selectNodes(Constants.NODE_OUT_PARAMETER_PATH);
				Map<String, String> outputParameterMap = (Map<String, String>) new LinkedHashMap<String, String>();
				
				for (Node nodeOutputParameter : nodeOutputParameters) {
					outputParameterMap.put(paramIndex++ + "",
							nodeOutputParameter.getText());
					parameter = new Parameter();
					parameter.setName(nodeOutputParameter.valueOf(new StringBuffer(Constants.XML_ATTR_SYMBOL).append(Constants.NODE_PARAMETER_ATTR_NAME).toString()));
					parameter.setType(nodeOutputParameter.valueOf(new StringBuffer(Constants.XML_ATTR_SYMBOL).append(Constants.NODE_PARAMETER_ATTR_TYPE).toString()));
					parameter.setValue(nodeOutputParameter.getText());
					outputParameterList.add(parameter);

				}
				request.setOutputParameters(outputParameterList);
				log.info("output parameter size " + outputParameterList);

				dbRequestList.add(request);
	
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dbRequestList;
	}

}
