package com.cba.component.dbos.handler;


/**
 *
 * @author sduraisamy
 */
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.apache.log4j.MDC;
import org.dom4j.Document;
import org.springframework.jms.core.JmsTemplate;

import com.cba.component.dbos.model.DBOSRequest;
import com.cba.component.dbos.model.DBOSResponse;
import com.cba.component.interfaces.IRequestProcessor;
import com.cba.trading.utils.JMSMessageCreator;
import com.ibm.disthubmq.impl.formats.OldEnvelop.payload.normal.reply;


public class DBOSRequestProcessor implements IRequestProcessor{

    static Logger log = Logger.getLogger(DBOSRequestProcessor.class.getName());


    private JmsTemplate jmsHandle;

    private String requestQueue;

    Map<String, JmsTemplate> jmsDestHandleMap;

    private DBOSResponseProcessor dbosResponseProcessor;

    private List<String> validSchemas;

	private DBRequestProcessor dbRequestProcessor;

	private String destQueue;

	private ExecutorService executor;
	private String shutdownWaitTimeout;
	
	private boolean processRunning;
	
		
	public boolean isProcessRunning() {
		return processRunning;
	}

	public void setProcessRunning(boolean processRunning) {
		this.processRunning = processRunning;
	}

	public String getShutdownWaitTimeout() {
		return shutdownWaitTimeout;
	}

	public void setShutdownWaitTimeout(String shutdownWaitTimeout) {
		this.shutdownWaitTimeout = shutdownWaitTimeout;
	}

	public ExecutorService getExecutor() {
		return executor;
	}

	public void setExecutor(ExecutorService executor) {
		this.executor = executor;
	}

	public List<String> getValidSchemas() {
		return validSchemas;
	}

	public void setValidSchemas(List<String> validSchemas) {
		this.validSchemas = validSchemas;
	}

	public DBOSResponseProcessor getDbosResponseProcessor() {
		return dbosResponseProcessor;
	}


	public void setDbosResponseProcessor(DBOSResponseProcessor dbosResponseProcessor) {
		this.dbosResponseProcessor = dbosResponseProcessor;
	}


	public DBRequestProcessor getDbRequestProcessor() {
		return dbRequestProcessor;
	}


	public void setDbRequestProcessor(DBRequestProcessor dbRequestProcessor) {
		this.dbRequestProcessor = dbRequestProcessor;
	}


    public Map<String, JmsTemplate> getJmsDestHandleMap() {
        return jmsDestHandleMap;
    }


	public void setJmsDestHandleMap(Map<String, JmsTemplate> jmsDestHandleMap) {
        this.jmsDestHandleMap = jmsDestHandleMap;
    }

    public JmsTemplate getJmsHandle() {
        return jmsHandle;
    }

    public void setJmsHandle(JmsTemplate jmsHandle) {
        this.jmsHandle = jmsHandle;
    }

    public String getRequestQueue() {
        return requestQueue;
    }

    public void setRequestQueue(String requestQueue) {
        this.requestQueue = requestQueue;
    }

    public void setDestQueue(String destQueue) {
        this.destQueue = destQueue;
    }

    public String getDestQueue() {
        return destQueue;
    }

    public void processRequest(String[] args) throws Exception {

        StringBuffer errorMessage = new StringBuffer();

        try
        {
        	if (jmsHandle == null) {
        		errorMessage.append("jmsHandle is null \n");
        	}
        	if (requestQueue == null) {
        		errorMessage.append("requestQueue is null \n");
        	}
        	if (errorMessage.length() > 0) {
        		throw new RuntimeException("Missing properties: \n" + errorMessage.toString());
        	}
        	setProcessRunning(true);
        	log.info("Shutdown Wait Timeout " + args[0]);
        	setShutdownWaitTimeout(args[0]);
            /**
             * Code to control losing messages while shutting down the component
             * - Shutdown hook is called when JVM is terminated
             */
            Runtime.getRuntime().addShutdownHook(new Thread() {

                public void run() {
                    try {
                    	log.info("Calling Shutdown hook in DBOSComponent");
                    	System.out.println("Calling Shutdown hook in DBOSComponent");
                        setProcessRunning(false);
                        getExecutor().awaitTermination(Integer.parseInt(getShutdownWaitTimeout()), TimeUnit.SECONDS);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                        log.info("Interrupting Thread Execution",e);
                    }
                }
            });

        	while (isProcessRunning()) {

        		DBOSRequest requestModel = null;
        		try {

        			log.info("Waiting for a request Message .....");
        			requestModel = (DBOSRequest) jmsHandle.receiveAndConvert(requestQueue);

        			log.info("Request Message Parsing Completed. Starting to Validate the Request");

        			getExecutor().execute(new ProcessRequestMessage(requestModel));

        		}catch (Exception e) {
        			log.error("\n\nException caught in processRequest while: \n" +
        					e.getMessage() +
        					"Please verify the Request. The request has been rejected by Component", e);

        		} finally {

        		}
        	}
        }catch(Exception e) {
        	log.error("\n\nException caught in processRequest main: \n" +
        			e.getMessage() +
        			"Please verify the Request. The request has been rejected by Component", e);

        } finally {
        	executor.shutdown();
        }

    }


    private class ProcessRequestMessage implements Runnable {

        private DBOSRequest dbosRequest;
        private String responseQueue;

        public ProcessRequestMessage() {
            super();
        }

        public ProcessRequestMessage(DBOSRequest dbosRequest) {
            // setRequestHolder(requestHolder);
            this.dbosRequest = dbosRequest;
            log.info("Logging DBOSRequest object" + dbosRequest.toString());
        }

        public void run() {

            StringBuffer errorMessage = new StringBuffer();

            String responseXML = "";

			long dbosresponseLen;

            if (jmsHandle == null) {
                errorMessage.append("jmsHandle is null\n");
            }
            /*if (requestQueue == null) {
                errorMessage.append("requestQueue is null\n");
            }
            if (errorMessage.length() > 0) {
                throw new RuntimeException("Missing properties: \n" + errorMessage.toString());
            }*/

            log.info("Started ProcessRequestMessage Method .....");
            MDC.put("requestId", dbosRequest.getRequestId());

            String currentThreadName = Thread.currentThread().getName();
            log.info("Worker Thread [ " + currentThreadName + " ] has started processing the Request");

            // Start Multi-threading using Spring Task Executor
            Document responseDocument = null;

            String queueName = null;

            try {


            	DBOSResponse dbosResponse = dbRequestProcessor.processDBOSRequest(dbosRequest);

            	responseXML = dbosResponseProcessor.toXml(dbosResponse);

                // Get the response queue from the Request
            	log.info("ReplyToQueue "+ dbosRequest.getReplyToQueue());
                if(dbosRequest.getReplyToQueue() != null) {
                	responseQueue = dbosRequest.getReplyToQueue();
                } else {
                	responseQueue = destQueue;
                }

            	
                // send the response
                dbosresponseLen=responseXML.trim().length();
                log.info("DBResponse Length" + dbosresponseLen );
                log.info("DBResponse is " + responseXML.trim());
               /* if( dbosresponseLen < 100000)
                	log.info("DBResponse is " + responseXML.trim());
                else
                	log.info("DBResponse Greater than 100000, Pls check the Glassfish Logs");*/


                log.info(" Correlation Id " + dbosRequest.getReplyCorrelationId());
                if (dbosRequest.getReplyCorrelationId() != null && dbosRequest.getReplyCorrelationId().trim().length() > 0) {

                	log.info("Entering reply with Correlation Id ******************************" + destQueue + "======" + dbosRequest.toString());

                	jmsHandle.send(responseQueue, new JMSMessageCreator(responseXML,dbosRequest.getReplyCorrelationId()));

                }
                else
                {

                	log.info("Entering reply without Correlation Id ");

                	jmsHandle.send(responseQueue, new JMSMessageCreator(responseXML));

                }

                    log.info("DBOSComponent has successfully responded to the Request with requestID [ " + dbosRequest.getRequestId() + " ] \n");
            } catch (Exception e) {

                log.error("\n\nException caught: \n" +
                        e.getMessage() +
                        "Please verify the transaction. The request has been rejected", e);


            } finally {
               // response = null;
            }
            //}

        }
    }
}
