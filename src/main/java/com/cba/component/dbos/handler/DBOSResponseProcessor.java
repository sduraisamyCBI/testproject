package com.cba.component.dbos.handler;
/**
 *
 * @author sduraisamy
 */
import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.Element;

import com.cba.component.dbos.model.DBOSResponse;
import com.cba.component.dbos.model.DBResponse;
import com.cba.component.dbos.model.Parameter;
import com.cba.component.dbos.utils.Constants;
import com.cba.trading.utils.XMLUtils;

public class DBOSResponseProcessor {

	static Logger log = Logger.getLogger(DBOSResponseProcessor.class.getName());

	public String toXml(DBOSResponse dbosResponse) throws Exception{

		Document document = XMLUtils.createXmlDocument(Constants.DBOS_REPLY);

		document.getRootElement().addAttribute(Constants.DBOS_REQUEST_ID,	dbosResponse.getRequestId());

		boolean dbosReplyHasError = false;

		if (dbosResponse.getSchema() != null && dbosResponse.getSchema().trim().length() > 0) {
			document.getRootElement().addAttribute(Constants.DB_SCHEMA,	dbosResponse.getSchema());
		}
		Element eltDBOSReplyCode = XMLUtils.addElement(document.getRootElement(), Constants.REPLY_CODE, "");

		Element eltDBOSReplyCodeDesc  = XMLUtils.addElement(document.getRootElement(), Constants.REPLY_CODE_DESC, "");


		for (DBResponse dbResponse : dbosResponse.getDbResponse()) {

			boolean dbReplyHasError = false;

			Element eltDBReplyCode = null;

			Element eltDBReplyCodeDesc = null;

			Element dbReply= XMLUtils.addElement(
					document.getRootElement(), Constants.DB_REPLY, "");

			if (dbResponse.getSchema() != null && dbResponse.getSchema().trim().length() > 0) {
				dbReply.addAttribute(Constants.DB_SCHEMA,	dbResponse.getSchema());
			}

			dbReply.addAttribute(Constants.OPERATION_ID, dbResponse.getOperationId());

			dbReply.addAttribute(Constants.OPERATION_NAMESPACE,	dbResponse.getOperationNameSpace());

			dbReply.addAttribute(Constants.OPERATION_TYPE, dbResponse.getOperationType());

			eltDBReplyCode = XMLUtils.addElement(dbReply, Constants.DB_REPLY_CODE, dbResponse.getReplyCode());

			eltDBReplyCodeDesc = XMLUtils.addElement(dbReply, Constants.DB_REPLY_CODE_DESC, dbResponse.getReplyCodeDesc());

			if (dbResponse.getReplyCode().equals(Constants.REPLY_CODE_ERROR)) {
				dbReplyHasError = true;
			}
			if (dbResponse.getNumberOfRows() != null) {

				XMLUtils.addElement(dbReply,Constants.NUMBER_OF_ROWS,dbResponse.getNumberOfRows()+"");
			}


			if (dbResponse.getOperationType().equalsIgnoreCase(Constants.OPERATION_TYPE_INSERT)) {

				//TODO Check if this block is required

			} else if (dbResponse.getOperationType().equalsIgnoreCase(Constants.OPERATION_TYPE_EXECUTE) || 
					dbResponse.getOperationType().equalsIgnoreCase(Constants.OPERATION_TYPE_EXECUTE_FUNCTION)) {
				if (dbResponse.getOutputParameters() != null && dbResponse.getOutputParameters().size() > 0)
				{

					Element resultSet =  XMLUtils.addElement(dbReply,Constants.NODE_RESULTSET,"");

					//populate all the input parameters
					if (dbResponse.getInputParameters() != null && dbResponse.getInputParameters().size() > 0) {
						//Element outParams =  XMLUtils.addElement(resultSet,"OutParams","");
						Element inParams  =  XMLUtils.addElement(resultSet,Constants.NODE_IN_PARAMS,"");
						for (Parameter parameter: dbResponse.getInputParameters())
						{
							Element param = XMLUtils.addElement(inParams, Constants.NODE_PARAM, parameter.getValue());
							
							if (parameter.getName() != null && !"".equals(parameter.getName().trim())) {
								
								param.addAttribute(Constants.NODE_PARAMETER_ATTR_NAME,parameter.getName());
								
							}
						}
					}
					log.info(" About to enter reply code success");
					if (dbResponse.getReplyCode().equals(Constants.REPLY_CODE_SUCCESS)) {
					
						log.info(" After entering to reply code success");
						Element outParams =  XMLUtils.addElement(resultSet, Constants.NODE_OUT_PARAMS, "");
						String outParamName = null;
						String outParamType = null;
						String outParamValue = null;
						//populate all the output parameters
						log.info("dbResponse.getOutputParameters() " +dbResponse.getOutputParameters().size() );
						for (Parameter parameter : dbResponse.getOutputParameters()) {

							Element param = null;
							outParamName = parameter.getName();
							outParamType = parameter.getType();
							outParamValue = parameter.getValue();
							log.info( "outParamName" + outParamName);
							log.info( "outParamType" + outParamType);
							log.info( "outParamValue" + outParamValue);
							
							// if parameter type attribute is available 
							if (outParamType != null && !"".equals(outParamType.trim())) {
								
								//if parameter type attribute value is "xml" or "XML"
								if (Constants.NODE_PARAMETER_TYPE_XML.equalsIgnoreCase(outParamType.trim())) {
									param = XMLUtils.addElement(outParams, Constants.NODE_PARAM,"");
									param.addAttribute(Constants.NODE_PARAMETER_ATTR_TYPE,outParamType);
									if (outParamValue != null && !"".equals(outParamValue.trim())) {
										param.add(XMLUtils.parse(outParamValue).getRootElement());
									}
								}
								// for other parameter types
								else
								{
									param = XMLUtils.addElement(outParams, Constants.NODE_PARAM, outParamValue != null ? outParamValue:"");
								}
							}
							// if parameter type attribute is available
							else
							{
								param = XMLUtils.addElement(outParams, Constants.NODE_PARAM,outParamValue != null ? outParamValue:"");	
							}
							

							if (outParamName != null && !"".equals(outParamName.trim())) {
								
								param.addAttribute(Constants.NODE_PARAMETER_ATTR_NAME,outParamName);

								//if the stored procedure returns an error status then
								//populate the replycode and replycodedesc in the corresponding DBREPLY with the stored procedure returned
								//status codes
								if (Constants.REPLY_CODE.toLowerCase().equals(outParamName.toLowerCase()) && !outParamValue.equals(Constants.REPLY_CODE_SUCCESS)){
									//boolean variable to hold the stored procedure error status - to be populated for the corresponding DBReply
									dbReplyHasError = true;
									//boolean variable to hold the stored procedure error status to be populated for DBOSReply
									dbosReplyHasError = true;
									eltDBReplyCode.setText(Constants.REPLY_CODE_ERROR);
								}

								if (dbReplyHasError && Constants.REPLY_CODE_DESC.toLowerCase().equals(outParamName.toLowerCase())) {
									eltDBReplyCodeDesc.setText(parameter.getValue());
								}
							}
						}
					}
				}

			} else if (dbResponse.getOperationType().equalsIgnoreCase(Constants.OPERATION_TYPE_SELECT)) {

				if (dbResponse.getResultSet() != null && dbResponse.getResultSet().size() > 0)
				{
					Element resultSet =  XMLUtils.addElement(dbReply,Constants.NODE_RESULTSET,"");
					if (dbResponse.getInputParameters() != null && dbResponse.getInputParameters().size() > 0) {
						
						Element inParams  =  XMLUtils.addElement(resultSet,Constants.NODE_IN_PARAMS,"");

						for (Parameter parameter: dbResponse.getInputParameters())
						{
							Element param = XMLUtils.addElement(inParams, Constants.NODE_PARAM, parameter.getValue());
							
							param.addAttribute(Constants.NODE_PARAMETER_ATTR_NAME,parameter.getName());
						}
					}
					for (String strResultSetXml : dbResponse.getResultSet()) {
						
						org.dom4j.Document docResultSet = XMLUtils.parse(strResultSetXml);
						
						resultSet.add(docResultSet.getRootElement());
					}
				}
			} else if (dbResponse.getOperationType().equalsIgnoreCase(Constants.OPERATION_TYPE_UPDATE)) {

				//TODO Check if this block is required
			} else if (dbResponse.getOperationType().equalsIgnoreCase(Constants.OPERATION_TYPE_DELETE)) {

				//TODO Check if this block is required
			}

		}

		//if one or more errors then include the original request as part of the response
		if (dbosReplyHasError) {

			Element eltOriginalRequest = XMLUtils.addElement(document.getRootElement(), Constants.ORIGINAL_REQUEST, "");			

			eltDBOSReplyCode.setText(Constants.REPLY_CODE_ERROR);

			eltDBOSReplyCodeDesc.setText(Constants.DBOSREPLY_ERROR_MSG);

			eltOriginalRequest.add(XMLUtils.parse(dbosResponse.getOriginalRequestXml()).getRootElement());
		}
		else
		{
			eltDBOSReplyCode.setText(Constants.REPLY_CODE_SUCCESS);

			eltDBOSReplyCodeDesc.setText(Constants.DBOSREPLY_SUCCESS_MSG);


		}
		return document.asXML();
	}
}
