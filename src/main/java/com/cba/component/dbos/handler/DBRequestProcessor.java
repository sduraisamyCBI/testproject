package com.cba.component.dbos.handler;
/**
 *
 * @author sduraisamy
 */
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.cba.component.dbos.dao.BaseDaoImpl;
import com.cba.component.dbos.dao.support.DataSourceContextHolder;
import com.cba.component.dbos.model.DBOSRequest;
import com.cba.component.dbos.model.DBOSResponse;
import com.cba.component.dbos.model.DBRequest;
import com.cba.component.dbos.model.DBResponse;
import com.cba.component.dbos.model.Parameter;
import com.cba.component.dbos.utils.Constants;
import com.cba.component.dbos.vo.BaseVO;
import com.cba.component.dbos.vo.ResponseVO;

public class DBRequestProcessor {

	static Logger log = Logger.getLogger(DBOSResponseProcessor.class.getName());

	private BaseDaoImpl baseDao;

	public BaseDaoImpl getBaseDao() {
		return baseDao;
	}

	public void setBaseDao(BaseDaoImpl baseDao) {
		this.baseDao = baseDao;
	}


	public DBOSResponse processDBOSRequest(DBOSRequest dbosRequest) throws Exception{

		DBOSResponse dbosResponse = new DBOSResponse();

		List<DBResponse> dbResponseList = new ArrayList<DBResponse>();
		if (dbosRequest != null) {

			if (dbosRequest.getDbRequest() != null) {
				BaseVO baseVO = null;

				DBResponse dbResponse = null;

				ResponseVO responseVO = null;

				String operationType = "";

				String schema = "";

				dbosResponse.setOriginalRequestXml(dbosRequest.getRequestXml());

				for (DBRequest dbRequest : dbosRequest.getDbRequest()) {

					baseVO = new BaseVO();
					dbResponse = new DBResponse();
					//IBATIS SQL operation id should be concatenated string of operationNameSpace and operationId 
					baseVO.setOperationId(new StringBuffer(dbRequest.getOperationNameSpace()).append(".").append(dbRequest.getOperationId()).toString());


					if (dbRequest.getSchema() != null && dbRequest.getSchema().length() > 0)
					{
						schema = dbRequest.getSchema();
					}
					else
					{
						schema = dbosRequest.getSchema();

					}
					DataSourceContextHolder.setTargetDataSource(schema);

					Map parameterClone = new LinkedHashMap();

					Map parameterMap = new LinkedHashMap();

					operationType = dbRequest.getOperationType();
					
					if (operationType.equalsIgnoreCase(Constants.OPERATION_TYPE_DELETE) || 
							operationType.equalsIgnoreCase(Constants.OPERATION_TYPE_SELECT) || 
							operationType.equalsIgnoreCase(Constants.OPERATION_TYPE_UPDATE)) 
					{
						//populate input parameters
						Parameter parameter = null;
						int index = 0;
						for (int i = 0 ; i < dbRequest.getInputParameters().size() ; i++)
						{
							parameter = dbRequest.getInputParameters().get(i);
							if (parameter.getType() != null && parameter.getType().equalsIgnoreCase(Constants.NODE_MULTIPLE_PARAMETER_TYPE_VALUE))
							{
								parameterClone.put(index++ + "", (ArrayList)parameter.getValues());
							}
							else
							{
								parameterClone.put(index++ +"",parameter.getValue());
							}
						}
						baseVO.setData(parameterClone);
						
					}
					//Operation Type DELETE
					if (operationType.equalsIgnoreCase(Constants.OPERATION_TYPE_DELETE)) {

						responseVO = baseDao.delete(baseVO);

						addDBResults(dbResponse,responseVO);

						addResponseDetails(dbRequest,dbResponse);

					}
					//Operation Type EXECUTE PROCEDURE
					else if (operationType.equalsIgnoreCase(Constants.OPERATION_TYPE_EXECUTE)) {
						int index = 0;
						//populate input parameters
						Parameter parameter = null;
						for (int i = 0 ; i < dbRequest.getInputParameters().size() ; i++)
						{
							parameter = dbRequest.getInputParameters().get(i);
							parameterClone.put(index +"",parameter.getValue());
							parameterMap.put(index++ +"",parameter.getValue());
						}
						baseVO.setData(parameterClone);

						responseVO =  baseDao.execute(baseVO);
						addDBResults(dbResponse,responseVO);
						dbResponse.setInParamsMap(parameterMap);
						
						//add the output parameters irrespective of whether input parameters exists or not
						dbResponse.setOutputParameters(dbRequest.getOutputParameters());
						
						if (dbRequest.getInputParameters() != null && dbRequest.getInputParameters().size() > 0) {

							dbResponse.setInputParameters(dbRequest.getInputParameters());	
	
							List<String> keys = new ArrayList<String>(parameterMap.keySet());

							if (dbResponse.getReplyCode().equals(Constants.REPLY_CODE_SUCCESS)) {
								for (String key: keys) {

									dbResponse.getOutParamsMap().remove(key);
								}


							}

						}
						//Operation Type EXECUTE FUNCTION
						if (dbResponse.getOutParamsMap() != null && dbResponse.getOutParamsMap().size() > 0)
						{
							//populate output params to dbResponse
							List<String> outParamKeys = new ArrayList<String>(dbResponse.getOutParamsMap().keySet());
							log.info("outParamKeys " + outParamKeys);
							log.info("dbResponse.getOutputParameters() " + dbResponse.getOutputParameters());
							log.info("dbResponse.getOutputParameters().size() " + dbResponse.getOutputParameters().size());
							int i = 0;
							for (String outKey: outParamKeys) {
								dbResponse.getOutputParameters().get(i++).setValue(dbResponse.getOutParamsMap().get(outKey));
							}
						}
						addResponseDetails(dbRequest,dbResponse);
					}
					else if (operationType.equalsIgnoreCase(Constants.OPERATION_TYPE_EXECUTE_FUNCTION)) {
						int index = 0;
						//populate input parameters
						Parameter parameter = null;
						for (int i = 0 ; i < dbRequest.getInputParameters().size() ; i++)
						{
							parameter = dbRequest.getInputParameters().get(i);
							parameterClone.put(++index +"",parameter.getValue());
							parameterMap.put(index +"",parameter.getValue());
						}
						baseVO.setData(parameterClone);

						responseVO =  baseDao.execute(baseVO);
						addDBResults(dbResponse,responseVO);
						dbResponse.setInParamsMap(parameterMap);
						
						//add the output parameters irrespective of whether input parameters exists or not
						dbResponse.setOutputParameters(dbRequest.getOutputParameters());
						
						if (dbRequest.getInputParameters() != null && dbRequest.getInputParameters().size() > 0) {

							dbResponse.setInputParameters(dbRequest.getInputParameters());	
	
							List<String> keys = new ArrayList<String>(parameterMap.keySet());
							if (dbResponse.getReplyCode().equals(Constants.REPLY_CODE_SUCCESS)) {
								
								for (String key: keys) 
								{

									dbResponse.getOutParamsMap().remove(key);
								}
							}	

						}

						if (dbResponse.getOutParamsMap() != null && dbResponse.getOutParamsMap().size() > 0)
						{
							//populate output params to dbResponse
							List<String> outParamKeys = new ArrayList<String>(dbResponse.getOutParamsMap().keySet());
							log.info("outParamKeys " + outParamKeys);
							log.info("dbResponse.getOutputParameters() " + dbResponse.getOutputParameters());
							log.info("dbResponse.getOutputParameters().size() " + dbResponse.getOutputParameters().size());
							int i = 0;
							for (String outKey: outParamKeys) {
								dbResponse.getOutputParameters().get(i++).setValue(dbResponse.getOutParamsMap().get(outKey));
							}
						}
						addResponseDetails(dbRequest,dbResponse);
					} 
					//Operation Type INSERT
					else if (operationType.equalsIgnoreCase(Constants.OPERATION_TYPE_INSERT)) {

						baseVO.setData(dbRequest.getValues());

						responseVO = baseDao.insert(baseVO);

						addDBResults(dbResponse,responseVO);

						addResponseDetails(dbRequest,dbResponse);
					} 
					//Operation Type SELECT
					else if (operationType.equalsIgnoreCase(Constants.OPERATION_TYPE_SELECT)) {

						responseVO = baseDao.select(baseVO);

						addDBResults(dbResponse,responseVO);

						addResponseDetails(dbRequest,dbResponse);
					} 
					//Operation Type UPDATE
					else if (operationType.equalsIgnoreCase(Constants.OPERATION_TYPE_UPDATE)) {

						responseVO = baseDao.update(baseVO);

						addDBResults(dbResponse,responseVO);

						addResponseDetails(dbRequest,dbResponse);

					}
					dbResponseList.add(dbResponse);
				}

				dbosResponse.setRequestId(dbosRequest.getRequestId());

				dbosResponse.setSchema(dbosRequest.getSchema());

				dbosResponse.setDbResponse(dbResponseList);
			}
		}

		return dbosResponse;
	}

	private DBResponse addResponseDetails(DBRequest dbRequest, DBResponse dbResponse){

		dbResponse.setOperationId(dbRequest.getOperationId());

		dbResponse.setOperationNameSpace(dbRequest.getOperationNameSpace());

		dbResponse.setSchema(dbRequest.getSchema());

		dbResponse.setOperationType(dbRequest.getOperationType());

		return dbResponse;
	}

	private DBResponse addDBResults(DBResponse dbResponse, ResponseVO responseVO){

		dbResponse.setReplyCode(responseVO.getReplyCode());

		dbResponse.setReplyCodeDesc(responseVO.getReplyCodeDesc());

		dbResponse.setNumberOfRows(responseVO.getNumberOfRows());

		dbResponse.setResultSet(responseVO.getResultSet());

		dbResponse.setOutParamsMap(responseVO.getOutParamsMap());

		return dbResponse;
	}
}