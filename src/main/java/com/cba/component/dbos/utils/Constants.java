package com.cba.component.dbos.utils;

public class Constants {
	
   public static final String DBREQUEST_XPATH_KEY = "dbRequest";
   
   public static final String OPERATION_ID = "operationId";
   
   public static final String OPERATION_TYPE = "operationType";
   
   public static final String OPERATION_NAMESPACE = "operationNameSpace";
   
   public static final String NODE_VALUE_PATH = "Values/Value";
   
   public static final String NODE_IN_PARAMETER_PATH = "Parameters[@type='IN']/Parameter";
   
   public static final String NODE_OUT_PARAMETER_PATH = "Parameters[@type='OUT']/Parameter";
   
   public static final String NODE_PARAMETER_ATTR_NAME= "name";
   
   public static final String NODE_PARAMETER_ATTR_TYPE= "type";
   
   public static final String NODE_PARAMETER_TYPE_XML= "xml";
      
   public static final String XML_ATTR_SYMBOL = "@";
   
   public static final String DBOS_REPLY = "DBOSReply";
   
   public static final String DBOS_REQUEST = "DBOSRequest";
   
   public static final String DB_REPLY = "DBReply";
   
   public static final String DB_REQUEST = "DBRequest";
   
   public static final String DBOS_REQUEST_ID = "RequestId";
   
   public static final String DB_REPLY_CODE = "ReplyCode";
   
   public static final String DB_REPLY_CODE_DESC = "ReplyCodeDesc";
   
   public static final String NUMBER_OF_ROWS = "NumberOfRows";
   
   public static final String NODE_IN_PARAMS = "InParams";
   
   public static final String NODE_OUT_PARAMS = "OutParams";
   
   public static final String NODE_PARAM = "Parameter";

   public static final String NODE_RESULTSET = "ResultSet";
   
   public static final String OPERATION_TYPE_EXECUTE = "execute";
   
   public static final String OPERATION_TYPE_EXECUTE_FUNCTION = "executeFunction";
   
   public static final String OPERATION_TYPE_SELECT = "select";
   
   public static final String OPERATION_TYPE_UPDATE = "update";
   
   public static final String OPERATION_TYPE_DELETE = "delete";
   
   public static final String OPERATION_TYPE_INSERT = "insert";
   
   public static final String DB_SCHEMA= "schema";
   
   public static final String REPLY_CODE = "ReplyCode";
   
   public static final String REPLY_CODE_DESC = "ReplyCodeDesc";
   
   public static final String REPLY_CODE_SUCCESS = "0";
   
   public static final String REPLY_CODE_ERROR = "-1";
   
   public static final String ORIGINAL_REQUEST = "OriginalRequest";
   
   public static final String DBOSREPLY_SUCCESS_MSG = "All the requests are processed successfuly.";
   
   public static final String DBOSREPLY_ERROR_MSG = "One or more requests could not be processed.";
   
   //Constants for multiple values for parameters - starts
   public static final String NODE_MULTIPLE_PARAMETER_VALUE_PATH = "Values/Value";
   
   public static final String NODE_MULTIPLE_PARAMETER_TYPE_VALUE = "multiple";
   //Constants for multiple values for parameters - ends
   
}

