package com.cba.component.dbos.main;

/**
 *
 * @author sduraisamy
 */

import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import com.cba.trading.utils.XMLUtils;
import com.cba.component.interfaces.IRequestProcessor;


/**
 * Starts the spring application<br>
 * Usage: RunDBOSComponent < springConfigFileName ><br>
 *
 * The config file must be a valid Spring config file.
 */
public class RunDBOSComponent {

    static Logger log = Logger.getLogger(RunDBOSComponent.class.getName());

    /**
     * Creates the application context from the spring config file
     * @param configFileName
     * @return XmlBeanFactory
     */
    private static ApplicationContext getApplicationContext(String configFileName) {

        ApplicationContext appContext = null;
        if (configFileName != null) {
            appContext = new FileSystemXmlApplicationContext(configFileName);
        } else {
            log.error("The config file was not specified.");

            log.error("Usage: DBOSComponent <configFileName> [<application args>]");
            log.error("Application exiting with status 1");
            System.exit(1);
        }

        return appContext;
    }

    /**
     * Load the config file and execute the main class of the application.<br>
     * This will forward the command line arguments to the run method.<br>
     *
     * If an exception is thrown by the application and not handled, <br>
     * it will be caught here and the application will exit.
     *
     * @param args
     */
    public static void main(String[] args) {
        try {
        	//create ibatis sqlmapconfig file with mappers
        	URL url = ClassLoader.getSystemResource("dbos.properties");
        	log.info("## URL ## " + url);
            createSqlMapConfigFile(url.getFile());



            ApplicationContext appContext = getApplicationContext(args.length > 0 ? args[0] : null);
            IRequestProcessor mainClass =(IRequestProcessor)  appContext.getBean("mainClass");
            // remove the first argument which is the config file
            // and pass the rest to the main class
            String[] mainArgs = new String[args.length - 1];
            System.arraycopy(args, 1, mainArgs, 0, args.length - 1);

            log.info("config file: " + args[0]);

            log.info("args to main class: " + Arrays.asList(mainArgs).toString());

            mainClass.processRequest(mainArgs);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Application caught an unhandled exception. Exiting with status 1 " + e.getMessage(), e);
            System.exit(1);
        }
    }

    public static void createSqlMapConfigFile(String filename) {

    	Document docRoot = null;
    	FileReader fileReader = null;
    	XMLWriter xmlWriter = null;
    	try
    	{
    			Properties properties = new Properties();
    			properties.load(new FileInputStream(filename));
    			log.info("## file name ## " + filename);
    			String templateFile= properties.getProperty("SQLMAPCONFIG_TEMPLATE");
    			String sqlMapFilePath = (String)properties.get("SQLMAP_XMLS_FOLDER");

    			log.info("## templateFile ## " + templateFile);
    			log.info("## sqlMapFilePath ## " + sqlMapFilePath);
    			File file = new File(templateFile);
    			fileReader = new FileReader(file);
    			docRoot= (new SAXReader()).read(fileReader);

    			String path = file.getAbsolutePath().substring(0,file.getAbsolutePath().lastIndexOf("\\"));


    			File xmlFolder = new File(sqlMapFilePath);
    			//locate mapper files folder
    			File[] xmlFiles = xmlFolder.listFiles(new FileFilter() {

    				@Override
    				public boolean accept(File file) {

    					return file.getName().endsWith(".xml");

    				}
    			});
    			Element sqlMapElement = null;
    			for(File xmlFile : xmlFiles) {
    				sqlMapElement = XMLUtils.addElement(docRoot.getRootElement(), "sqlMap","");
    				sqlMapElement.addAttribute("url", "file:///" + xmlFile.getPath());
    			}
    			OutputFormat format = OutputFormat.createPrettyPrint();
    	        xmlWriter = new XMLWriter(new FileWriter( path + "/" + (String) properties.getProperty("SQLMAPCONFIG_FILENAME")),format);
    	        xmlWriter.write( docRoot );


    	}catch(DocumentException docException) {
    		docException.printStackTrace();
    		log.error(docException.getMessage());
    	}catch(FileNotFoundException fileNotFoundException) {
    		fileNotFoundException.printStackTrace();
    		log.error(fileNotFoundException.getMessage());
    	}catch(Exception exception) {
    		exception.printStackTrace();
    		log.error(exception.getMessage());
    	}finally
    	{
    		try
    		{
	        xmlWriter.close();
            fileReader.close();
    		}catch(IOException ioException){
    			log.error(ioException.getMessage());
    		}

    	}

    }
}