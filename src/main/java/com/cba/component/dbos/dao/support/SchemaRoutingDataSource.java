package com.cba.component.dbos.dao.support;
/**
*
* @author sduraisamy
*/
import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

public class SchemaRoutingDataSource extends AbstractRoutingDataSource{
	   @Override
	   protected Object determineCurrentLookupKey() {
	      return DataSourceContextHolder.getTargetDataSource();
	   }

}
