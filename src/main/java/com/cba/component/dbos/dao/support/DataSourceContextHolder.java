package com.cba.component.dbos.dao.support;
/**
*
* @author sduraisamy
*/
public class DataSourceContextHolder {
	private static final ThreadLocal<String> contextHolder = 
		new ThreadLocal<String>();
	
	public static void setTargetDataSource(String targetDataSource){
		contextHolder.set(targetDataSource);
	}
	
	public static String getTargetDataSource(){
		return (String) contextHolder.get();
	}
	
	public static void resetDefaultDataSource(){
		contextHolder.remove();
	}
}

