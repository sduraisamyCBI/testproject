package com.cba.component.dbos.dao;

/**
 *
 * @author sduraisamy
 */
import java.sql.SQLException;
import java.util.List;

import org.apache.log4j.Logger;

import com.cba.component.dbos.utils.Constants;
import com.cba.component.dbos.vo.BaseVO;
import com.cba.component.dbos.vo.ResponseVO;
import com.cba.component.interfaces.IBaseDao;
import com.ibatis.sqlmap.client.SqlMapClient;

public class BaseDaoImpl implements IBaseDao {

	static Logger log = Logger.getLogger(BaseDaoImpl.class.getName());

	private SqlMapClient sqlMapClient;

	public SqlMapClient getSqlMapClient() {
		return sqlMapClient;
	}

	public void setSqlMapClient(SqlMapClient sqlMapClient) {
		this.sqlMapClient = sqlMapClient;
	}

	public ResponseVO insert(BaseVO baseVO) {

		ResponseVO responseVO = new ResponseVO();
		try {

			log.info("BaseDaoImpl insert method " + baseVO);

			Object object = getSqlMapClient().insert(baseVO.getOperationId(),
					baseVO.getData());

			responseVO.setReplyCode(Constants.REPLY_CODE_SUCCESS);

			responseVO.setReplyCodeDesc("Insertion Successful.");

		} catch (SQLException sqlException) {

			log.error(sqlException.getMessage(), sqlException);

			responseVO.setReplyCode(Constants.REPLY_CODE_ERROR);

			responseVO.setReplyCodeDesc(sqlException.getMessage());

		} catch (Exception exception) {
			log.error(exception.getMessage(), exception);

			responseVO.setReplyCode(Constants.REPLY_CODE_ERROR);

			responseVO.setReplyCodeDesc(exception.getMessage());

		}

		return responseVO;
	}

	public ResponseVO select(BaseVO baseVO) {

		ResponseVO responseVO = new ResponseVO();
		try {
			log.info("BaseDaoImpl select method " + baseVO);

			List list = null;
			log.info("BaseDaoImpl select method getData " + baseVO.getData());
			//log.info("BaseDaoImpl select method getData " + (baseVO.getData() != null) ? baseVO.getData().size().toString() : "Size is null");
			if (baseVO.getData() != null && baseVO.getData().size() == 0) {

				list = (List) getSqlMapClient().queryForList(
						baseVO.getOperationId());

			} else {

				list = (List) getSqlMapClient().queryForList(
						baseVO.getOperationId(), baseVO.getData());

			}
			responseVO.setNumberOfRows((list != null) ? list.size() : 0);

			responseVO.setReplyCode(Constants.REPLY_CODE_SUCCESS);

			responseVO.setReplyCodeDesc("Selection Successful.");

			responseVO.setResultSet(list);

			log.info("BaseDaoImpl select - Return data " + list);

		} catch (SQLException sqlException) {

			log.error(sqlException.getMessage(), sqlException);

			responseVO.setReplyCode(Constants.REPLY_CODE_ERROR);

			responseVO.setReplyCodeDesc(sqlException.getMessage());

		} catch (Exception exception) {
			log.error(exception.getMessage(), exception);

			responseVO.setReplyCode(Constants.REPLY_CODE_ERROR);

			responseVO.setReplyCodeDesc(exception.getMessage());
		}

		return responseVO;
	}

	public ResponseVO update(BaseVO baseVO) {
		ResponseVO responseVO = new ResponseVO();
		try {
			log.info("BaseDaoImpl update method " + baseVO);

			int rows = getSqlMapClient().update(baseVO.getOperationId(),
					baseVO.getData());

			responseVO.setNumberOfRows(rows);

			responseVO.setReplyCode(Constants.REPLY_CODE_SUCCESS);

			responseVO.setReplyCodeDesc("Update Sucecssful.");

			log.info("BaseDaoImpl update status " + rows);

		} catch (SQLException sqlException) {
			log.error(sqlException.getMessage(), sqlException);

			responseVO.setReplyCode(Constants.REPLY_CODE_ERROR);

			responseVO.setReplyCodeDesc(sqlException.getMessage());

		} catch (Exception exception) {
			log.error(exception.getMessage(), exception);

			responseVO.setReplyCode(Constants.REPLY_CODE_ERROR);

			responseVO.setReplyCodeDesc(exception.getMessage());
		}

		return responseVO;
	}

	public ResponseVO delete(BaseVO baseVO) {

		ResponseVO responseVO = new ResponseVO();
		try {
			log.info("BaseDaoImpl delete method " + baseVO);

			int rows = getSqlMapClient().delete(baseVO.getOperationId(),
					baseVO.getData());

			responseVO.setNumberOfRows(rows);

			responseVO.setReplyCode(Constants.REPLY_CODE_SUCCESS);

			responseVO.setReplyCodeDesc("Deletion Sucecssful.");

			log.info("BaseDaoImpl delete status " + rows);

		} catch (SQLException sqlException) {

			log.error(sqlException.getMessage(), sqlException);

			responseVO.setReplyCode(Constants.REPLY_CODE_ERROR);

			responseVO.setReplyCodeDesc(sqlException.getMessage());

		} catch (Exception exception) {

			log.error(exception.getMessage(), exception);

			responseVO.setReplyCode(Constants.REPLY_CODE_ERROR);

			responseVO.setReplyCodeDesc(exception.getMessage());

		}

		return responseVO;
	}

	public ResponseVO execute(BaseVO baseVO) {

		ResponseVO responseVO = new ResponseVO();

		try {
			log.info("BaseDaoImpl execute method" + baseVO);

			int status = getSqlMapClient().update(baseVO.getOperationId(),
					baseVO.getData());

			responseVO.setReplyCode(Constants.REPLY_CODE_SUCCESS);

			responseVO
					.setReplyCodeDesc("Stored Procedure executed successfully.");

			responseVO.setOutParamsMap(baseVO.getData());

			log.info("BaseDaoImpl execute - Return data " + baseVO.getData());

		} catch (SQLException sqlException) {

			log.error(sqlException.getMessage(), sqlException);

			responseVO.setReplyCode(Constants.REPLY_CODE_ERROR);

			responseVO.setReplyCodeDesc(sqlException.getMessage());

		} catch (Exception exception) {
			log.error(exception.getMessage(), exception);

			responseVO.setReplyCode(Constants.REPLY_CODE_ERROR);

			responseVO.setReplyCodeDesc(exception.getMessage());
		}
		return responseVO;
	}

}
