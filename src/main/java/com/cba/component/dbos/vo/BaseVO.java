package com.cba.component.dbos.vo;

import java.util.Map;

public class BaseVO implements IBaseVO {

	static final long serialVersionUID = 2;
	
	private String operationId;

	private Map data;

	public String getOperationId() {
		return operationId;
	}

	public void setOperationId(String operationId) {
		this.operationId = operationId;
	}

	public Map getData() {
		return data;
	}

	public void setData(Map data) {
		this.data = data;
	}

	public String toString() {
		return new StringBuffer("operationId : ").append(operationId)
				.append("\n").append("Input Data : " + data).append("\n")
				.toString();
	}
}
