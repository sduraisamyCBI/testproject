package com.cba.component.dbos.vo;

import java.util.List;
import java.util.Map;

public class ResponseVO implements IBaseVO {
	
	static final long serialVersionUID = 11111;
	
	private String replyCode;

	private String replyCodeDesc;

	private Integer numberOfRows;

	private List<String> resultSet;
	
	private Map<String,String> outParamsMap;

	public Map<String, String> getOutParamsMap() {
		return outParamsMap;
	}

	public void setOutParamsMap(Map<String, String> outParamsMap) {
		this.outParamsMap = outParamsMap;
	}

	public String getReplyCode() {
		return replyCode;
	}

	public void setReplyCode(String replyCode) {
		this.replyCode = replyCode;
	}

	public String getReplyCodeDesc() {
		return replyCodeDesc;
	}

	public void setReplyCodeDesc(String replyCodeDesc) {
		this.replyCodeDesc = replyCodeDesc;
	}

	public Integer getNumberOfRows() {
		return numberOfRows;
	}

	public void setNumberOfRows(Integer numberOfRows) {
		this.numberOfRows = numberOfRows;
	}

	public List<String> getResultSet() {
		return resultSet;
	}

	public void setResultSet(List<String> resultSet) {
		this.resultSet = resultSet;
	}

}
