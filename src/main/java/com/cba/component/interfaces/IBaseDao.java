package com.cba.component.interfaces;

import java.util.Map;

import com.cba.component.dbos.model.DBResponse;
import com.cba.component.dbos.vo.BaseVO;
import com.cba.component.dbos.vo.IBaseVO;
import com.cba.component.dbos.vo.ResponseVO;


public interface IBaseDao {
   public IBaseVO insert(BaseVO baseVO);
   public IBaseVO select(BaseVO baseVO);
   public IBaseVO update(BaseVO baseVO);
   public IBaseVO delete(BaseVO baseVO);
   public IBaseVO execute(BaseVO baseVO);
}
