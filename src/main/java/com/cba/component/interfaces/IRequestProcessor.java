package com.cba.component.interfaces;

public interface IRequestProcessor {
	public void processRequest(String[] args) throws Exception; 
}
