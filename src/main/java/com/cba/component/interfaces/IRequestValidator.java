package com.cba.component.interfaces;

import com.cba.trading.model.AbstractModel;



public interface IRequestValidator {
   public String validate(AbstractModel model);
}
